# Base image
FROM node:12.18.0-alpine

# Set working directory
WORKDIR /app

# Add `./node_modules/.bin` to $PATH
ENV PATH ./node_modules/.bin:$PATH

# Install and cache app dependencies
COPY package.json package-lock.json ./
RUN npm install

# Add app
COPY . .

# Start app
CMD ["ng", "serve", "--host", "0.0.0.0"]
