import { NewDwellingsCompletionService } from '@core/services/new-dwellings-completion.service';
import {Component, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import * as Highcharts from 'highcharts';
import * as moment from 'moment';

@Component({
  selector: 'app-new-dwellings-completion',
  templateUrl: './new-dwellings-completion.component.html',
  styleUrls: ['new-dwellings-completion.component.css']
})
export class NewDwellingsCompletionComponent implements OnInit {

  /**
   * Constructor of New dwellings completion Graph
   * @param newDwellingsCompletionService New dwellings completion service
   */
  constructor(private newDwellingsCompletionService: NewDwellingsCompletionService) {
    newDwellingsCompletionService.get(0);
    newDwellingsCompletionService.get(1);
    newDwellingsCompletionService.get(2);
    newDwellingsCompletionService.get(3);
  }


  public highcharts: any;
  public chartOptions: any;
  public chart;
  private data = [
    ['Firefox', 45.0],
    ['IE', 26.8],
    ['Safari', 8.5],
    ['Opera', 6.2],
    ['Others', 0.7]
  ];

  private selectedYearFilter = moment().format('YYYY');

  private yearFilterOption = [];

  private dataAll = null;
  private dataSingle = null;
  private dataScheme = null;
  private dataApartment = null;

  /*
   * A lifecycle hook that is called after Angular has initialized all data-bound properties of a directive.
   */
  ngOnInit(): void {
    this.highcharts = Highcharts;
    this.chartOptions = {
      chart: {
        plotBorderWidth: null,
        plotShadow: false
      },
      title: {
        text: 'New Dwelling Completions by Type of House'
      },
      subtitle: {
        text: '"Source: https://data.gov.ie"'
      },
      credits: {
        enabled: false
      },
      tooltip: {
        valueSuffix: ''
      },
      plotOptions : {
        pie: {
           allowPointSelect: true,
           cursor: 'pointer',
           dataLabels: {
              enabled: true,
              format: '<b>{point.name}</b>: {point.percentage:.1f}%',
           }
        }
      },
      series: [{
        type: 'pie',
        data: this.data
      }]
    };
    /*
    * The 'renderTo' property refers a DOM id (for instance a div id). The graph will render
    * 'inside' the referenced item.
     */
    this.chart = Highcharts.chart('propertyPriceContainer', this.chartOptions);


    this.newDwellingsCompletionService.datasets.subscribe(
      (response) => {
        if (response) {
          console.log('data :', response);
          console.log('type :', response.filtersApplied['Type of House']);
          switch (response.filtersApplied['Type of House']) {
            case 'All house types':
              this.dataAll = response.data;
              break;
            case 'Single house':
              this.dataSingle = response.data;
              break;
            case 'Scheme house':
              this.dataScheme = response.data;
              break;
            case 'Apartment':
              this.dataApartment = response.data;
              break;
            default:
              break;
          }

          const data = [];
          const years = this.yearFilterOption;

          response.data.map((d) => {
            const year = d.x;
            if (years.indexOf(year) === -1) {
              years.push(year);
            }

            if (response.filtersApplied['Type of House'] === 'Apartment') {
              this.updateData();
            }
          });


          this.yearFilterOption = years;
          /**
           * This is where we update the whole graph:
           * We update the categories with our categories
           * We update the dataseries with our new data
           * We then redraw our graphe.
           *
           */
          this.chartOptions.series[0].data = this.data;
          this.chart = Highcharts.chart('newDwellingsCompletionContainer', this.chartOptions);
        }
      }
    );
  }

  updateData() {
    const data = [];

    if (this.dataSingle != null) {
      const item = this.dataSingle.find(val => val.x === this.selectedYearFilter);
      if (item != null) {
        data.push(item ? ['Single house', item.y[0]] : ['Single house', 0]);
      }
    }

    if (this.dataScheme != null) {
      const item = this.dataScheme.find(val => val.x === this.selectedYearFilter);
      if (item != null) {
        data.push(item ? ['Scheme house', item.y[0]] : ['Scheme house', 0]);
      }
    }

    if (this.dataApartment != null) {
      const item = this.dataApartment.find(val => val.x === this.selectedYearFilter);
      if (item != null) {
        data.push(item ? ['Apartment', item.y[0]] : ['Apartment', 0]);
      }
    }

    this.data = data;
  }

  onFilterChangeYear(e) {
    this.selectedYearFilter = e.value;

    this.updateData();

    this.chartOptions.series[0].data = this.data;
    this.chart = Highcharts.chart('newDwellingsCompletionContainer', this.chartOptions);
  }
}
