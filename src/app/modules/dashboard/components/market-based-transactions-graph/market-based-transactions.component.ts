import {PropertyPriceService} from '@core/services/property-price.service';
import {Component, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import * as Highcharts from 'highcharts';
import * as moment from 'moment';
import { MarketBasedTransactionsService } from '@app/core/services/market-based-transactions.service';

@Component({
  selector: 'app-market-based-transactions',
  templateUrl: './market-based-transactions.component.html',
  styleUrls: ['market-based-transactions.component.css']
})
export class MarketBasedTransactionsComponent implements OnInit {


  /**
   * Constructor of Market Based Transactions Graph
   * @param propertyPriceService  Property price service
   */
  constructor(private marketBasedTransactionsService: MarketBasedTransactionsService) {
    marketBasedTransactionsService.getFilters();
    marketBasedTransactionsService.get(0, 0, 0, 0);
  }

  public highcharts: any;
  public chartOptions: any;
  public chart;
  private categories = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  private data = [
    {
      name: 'Tokyo',
      data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
    },
    {
      name: 'New York',
      data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
    },
    {
      name: 'Berlin',
      data: [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0]
    },
    {
      name: 'London',
      data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
    }
  ];


  public selectedFirstFilter = 0;
  public selectedSecondFilter = 0;
  public selectedThirdFilter = 0;
  public selectedFourthFilter = 0;

  public firstFilterName = '';
  public firstFilterOption = new Map<string, string>();

  public secondFilterName = '';
  public secondFilterOption = new Map<string, string>();

  public thirdFilterName = '';
  public thirdFilterOption = new Map<string, string>();

  public fourthFilterName = '';
  public fourthFilterOption = new Map<string, string>();



  ngOnInit() {
    this.highcharts = Highcharts;
    this.chartOptions = {
      chart: {
        zoomType: 'xy'
      },
      title: {
        text: 'Market based non household transactions'
      },
      subtitle: {
        text: '"Source: https://data.gov.ie"'
      },
      xAxis: {
        categories: this.categories,
      },
      yAxis: [
        { // Primary yAxis
            labels: {
               format: '{value}',
               style: {
                color: Highcharts.getOptions().colors[1]
               }
            },
            title: {
               text: 'Quantity',
               style: {
                  color: Highcharts.getOptions().colors[1]
               }
            }
         },
         { // Secondary yAxis
            title: {
               text: 'Price',
               style: {
                  color: Highcharts.getOptions().colors[0]
               }
            },
            labels: {
               format: '{value} €',
            },
            opposite: true
         }
     ],
      credits: {
        enabled: false
      },
      tooltip: {
        shared: true
      },
      series: this.data
    };
    this.chart = Highcharts.chart('marketBasedTransactionsContainer', this.chartOptions);
    this.marketBasedTransactionsService.filters.subscribe(
      (response) => {
        if (response && response.length > 0) {
          this.firstFilterName = response[0].name;
          this.firstFilterOption = response[0].values;

          this.secondFilterName = response[1].name;
          this.secondFilterOption = response[1].values;

          this.thirdFilterName = response[2].name;
          this.thirdFilterOption = response[2].values;

          this.fourthFilterName = response[3].name;
          this.fourthFilterOption = response[3].values;
        }
      }
    );

    this.marketBasedTransactionsService.datasets.subscribe(
      (response) => {
        if (response) {
          const data = [];
          const categories = [];

          response.metrics.map((m) => {
            data.push({name: m.name, data: []});
          });

          console.log('Response :::', response.data);

          response.data.map((d) => {

            categories.push(d.x);
            d.y.map((v, index) => {
             if (data[index]) {
                data[index].data.push(v ? v : 0);
                if (index === 0) {
                    data[index].type = 'spline';
                    data[index].tooltip = { valueSuffix: ' ' };
                } else {
                    data[index].type = 'column';
                    data[index].yAxis = 1;
                }
             }
            });
          });

          this.categories = categories;
          this.data = data;
          console.log(categories);
          console.log(data);
          this.chartOptions.series = this.data;
          this.chartOptions.xAxis.categories = this.categories;
          this.chart = Highcharts.chart('marketBasedTransactionsContainer', this.chartOptions);
        }
      }
    );
  }

  onFilterChangeDwelling(e) {
    this.onFilterChange(e.value, this.selectedSecondFilter, this.selectedThirdFilter, this.selectedFourthFilter);
  }

  onFilterChangeParticipant(e) {
    this.onFilterChange(this.selectedFirstFilter, e.value, this.selectedThirdFilter, this.selectedFourthFilter);
  }

  onFilterChangeStamp(e) {
    this.onFilterChange(this.selectedFirstFilter, this.selectedSecondFilter, e.value, this.selectedFourthFilter);
  }

  onFilterChangeNACE(e) {
    this.onFilterChange(this.selectedFirstFilter, this.selectedSecondFilter, this.selectedThirdFilter, e.value);
  }


  onFilterChange(e, ee, eee, eeee) {
    this.marketBasedTransactionsService.get(e, ee, eee, eeee);
    this.chartOptions.series = this.data;
    this.chartOptions.xAxis.categories = this.categories;
    this.chart = Highcharts.chart('marketBasedTransactionsContainer', this.chartOptions);
  }

}
