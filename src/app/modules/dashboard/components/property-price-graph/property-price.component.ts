import {PropertyPriceService} from '@core/services/property-price.service';
import {Component, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import * as Highcharts from 'highcharts';
import * as moment from 'moment';

@Component({
  selector: 'app-property-price',
  templateUrl: './property-price.component.html',
  styleUrls: ['property-price.component.css']
})
export class PropertyPriceComponent implements OnInit {


  /**
   * Constructor of Property Price Graph
   * @param propertyPriceService  Property price service
   */
  constructor(private propertyPriceService: PropertyPriceService) {
    propertyPriceService.getFilters();
    propertyPriceService.get(0);
  }


  public highcharts: any;
  public chartOptions: any;
  public chart;
  private categories = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

   private data = [
      {
      name: 'Tokyo',
      data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
      },
      {
      name: 'New York',
      data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
      },
      {
      name: 'Berlin',
         data: [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0]
      },
      {
      name: 'London',
         data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
      }
   ];

  private selectedFilter = 0;

  private selectedYear = moment().format('YYYY');

  private filterOption = new Map<string, string>();

  private yearOption = [];

  private currentData = null;

  /*
   * A lifecycle hook that is called after Angular has initialized all data-bound properties of a directive.
   */
  ngOnInit(): void {
    this.highcharts = Highcharts;
    this.chartOptions = {
      chart: {
        type: 'spline'
      },
      title: {
        text: 'Property Price Evolution'
      },
      subtitle: {
        text: '"Source: https://data.gov.ie"'
      },
      xAxis: {
        categories: this.categories
      },
      yAxis: {
        title: {
          text: 'Percentage'
        }
      },
      credits: {
        enabled: false
      },
      tooltip: {
        valueSuffix: '%'
      },
      series: this.data
    };
    /*
    * The 'renderTo' property refers a DOM id (for instance a div id). The graph will render
    * 'inside' the referenced item.
     */
    this.chart = Highcharts.chart('propertyPriceContainer', this.chartOptions);
    this.propertyPriceService.filters.subscribe(
      (response) => {
        if (response && response.length > 0) {
          this.filterOption = response[0].values;
        }
      }
    );

    this.propertyPriceService.datasets.subscribe(
      (response) => {
        if (response) {
          const data = [];
          const categories = [];
          const years = [];

          this.currentData = response;

          response.metrics.map((m) => {
            data.push({name: m.name, data: []});
          });

          response.data.map((d) => {
            const year = d.x.split('M')[0];
            const month = moment().month(d.x.split('M')[1]).format('MMM');
            if (years.indexOf(year) === -1) {
              years.push(year);
            }
            if (parseInt(year, 10) >= parseInt(this.selectedYear, 10)) {
              categories.push(year + ' ' + month);
              d.y.map((v, index) => {
                if (data[index]) {
                  data[index].data.push(v ? v : 0);
                }
              });
            }
          });

          this.categories = categories;
          this.data = data;
          this.yearOption = years;
          /**
           * This is where we update the whole graph:
           * We update the categories with our categories
           * We update the dataseries with our new data
           * We then redraw our graphe.
           *
           */
          this.chartOptions.series = this.data;
          this.chartOptions.xAxis.categories = this.categories;
          this.chart = Highcharts.chart('propertyPriceContainer', this.chartOptions);
        }
      }
    );
  }

  onFilterChange(e) {
    this.propertyPriceService.get(e.value);
    this.selectedFilter = e.value;
    this.chartOptions.series = this.data;
    this.chartOptions.xAxis.categories = this.categories;
    this.chart = Highcharts.chart('propertyPriceContainer', this.chartOptions);
  }

  onFilterYearChange(e) {
    const data = [];
    const categories = [];

    this.selectedYear = e.value;

    this.currentData.metrics.map((m) => {
      data.push({name: m.name, data: []});
    });


    this.currentData.data.map((d) => {
      const year = d.x.split('M')[0];
      const month = moment().month(d.x.split('M')[1]).format('MMM');

      if (parseInt(year, 10) >= parseInt(this.selectedYear, 10)) {
        categories.push(year + ' ' + month);
        d.y.map((v, index) => {
          if (data[index]) {
            data[index].data.push(v ? v : 0);
          }
        });
      }
    });

    this.categories = categories;
    this.data = data;

    this.chartOptions.series = this.data;
    this.chartOptions.xAxis.categories = this.categories;
    this.chart = Highcharts.chart('propertyPriceContainer', this.chartOptions);
  }
}
