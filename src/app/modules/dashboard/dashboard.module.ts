import { SharedModule } from '@shared/shared.module';
import { NgModule } from '@angular/core';
import { DahsboardComponent } from './components/dashboard-page/dashboard.component';
import { HighchartsChartComponent } from 'highcharts-angular';
import { PropertyPriceComponent } from './components/property-price-graph/property-price.component';
import {DwellingSalesVolumeGraphComponent} from '@modules/dashboard/components/dwelling-sales-volume-graph/dwelling-sales-volume-graph.component';
import { MarketBasedTransactionsComponent } from './components/market-based-transactions-graph/market-based-transactions.component';
import { NewDwellingsCompletionComponent } from './components/new-dwellings-completion-graph/new-dwellings-completion.component';

@NgModule({
  declarations: [DahsboardComponent, PropertyPriceComponent, HighchartsChartComponent, DwellingSalesVolumeGraphComponent,
    MarketBasedTransactionsComponent, NewDwellingsCompletionComponent],
  imports: [
    SharedModule,
  ]
})
export class DashboardModule { }
