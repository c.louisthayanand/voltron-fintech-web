import { PropertyPriceService } from './services/property-price.service';
import { VoltronMaterialModule } from '@modules/material/voltron-material.module';
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HttpClientModule } from '@angular/common/http';
import {DwellingSalesVolumeService} from '@core/services/dwelling-sales-volume.service';
import { MarketBasedTransactionsService } from './services/market-based-transactions.service';
import { NewDwellingsCompletionService } from './services/new-dwellings-completion.service';


@NgModule({
  declarations: [NavbarComponent],
  imports: [
    HttpClientModule,
    VoltronMaterialModule
  ],
  exports: [NavbarComponent],
  providers: [
    PropertyPriceService,
    DwellingSalesVolumeService,
    MarketBasedTransactionsService,
    NewDwellingsCompletionService
  ]
})
export class CoreModule {

  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error('CoreModule is already loaded.');
    }
  }

}
