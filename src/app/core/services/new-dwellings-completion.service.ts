import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {FilteredDataset} from '@shared/models/filtered-dataset.model';
import {Filter} from '@shared/models/filter.model';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '@env/environment';

@Injectable()
export class NewDwellingsCompletionService {


  private _datasets: BehaviorSubject<FilteredDataset> = new BehaviorSubject(new FilteredDataset({}));
  private _filters: BehaviorSubject<Filter[]> = new BehaviorSubject([]);

  public readonly datasets: Observable<FilteredDataset> = this._datasets.asObservable();
  public readonly filters: Observable<Filter[]> = this._filters.asObservable();

  constructor(private http: HttpClient) { }

  public get(typeofHouseFilterIndex: number) {
    let params = new HttpParams();
    params = params.append('q', 'TypeofHouse:' + typeofHouseFilterIndex);
    params = params.append('epPoints', '5');
    params = params.append('epMethod', 'Linear');
    return this.http.get<FilteredDataset>(`${environment.endpoint}/newDwellingsCompletion`, {params})
      .toPromise()
      .then(
        (data) => {
          this._datasets.next(new FilteredDataset(data));

        },
        (error) => {
          console.log(error);
          return error;
        }
      );
  }

  public getFilters() {
    return this.http.get<Filter[]>(`${environment.endpoint}/newDwellingsCompletion/possibleFilters`)
      .toPromise()
      .then(
        (data) => {
          this._filters.next(data);
        },
        (error) => {
          console.log(error);
          return error;
        }
      );
  }
}
