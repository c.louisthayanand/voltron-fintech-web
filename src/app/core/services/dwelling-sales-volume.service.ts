import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {FilteredDataset} from '@shared/models/filtered-dataset.model';
import {Filter} from '@shared/models/filter.model';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '@env/environment';

@Injectable()
export class DwellingSalesVolumeService {


  private _datasets: BehaviorSubject<FilteredDataset> = new BehaviorSubject(new FilteredDataset({}));
  private _filters: BehaviorSubject<Filter[]> = new BehaviorSubject([]);

  public readonly datasets: Observable<FilteredDataset> = this._datasets.asObservable();
  public readonly filters: Observable<Filter[]> = this._filters.asObservable();

  constructor(private http: HttpClient) { }

  public get(dwellingStatusFilterIndex: number, stampDutyEventFilterIndex: number, RPPIRegionFilterIndex: number, buyerTypeFilterIndex) {
    let params = new HttpParams();
    let query = 'DwellingStatus:' + dwellingStatusFilterIndex;
    query += ';StampDutyEvents:' + stampDutyEventFilterIndex;
    query += ';RPPIRegion:' + RPPIRegionFilterIndex;
    query += ';TypeofBuyer:' + buyerTypeFilterIndex;
    params = params.append('q', query);
    params = params.append('epPoints', '10');
    params = params.append('epMethod', 'Linear');
    return this.http.get<FilteredDataset>(`${environment.endpoint}/dwellingSalesVolume`, {params})
      .toPromise()
      .then(
        (data) => {
          this._datasets.next(new FilteredDataset(data));

        },
        (error) => {
          console.log(error);
          return error;
        }
      );
  }

  public getFilters() {
    return this.http.get<Filter[]>(`${environment.endpoint}/dwellingSalesVolume/possibleFilters`)
      .toPromise()
      .then(
        (data) => {
          this._filters.next(data);
        },
        (error) => {
          console.log(error);
          return error;
        }
      );
  }
}
