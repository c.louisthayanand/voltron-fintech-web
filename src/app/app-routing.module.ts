import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DahsboardComponent } from './modules/dashboard/components/dashboard-page/dashboard.component';


const routes: Routes = [
  {path: 'dashboard', component: DahsboardComponent},
  {path: '**', redirectTo: '/dashboard'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
