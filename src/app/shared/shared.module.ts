import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VoltronMaterialModule } from '../modules/material/voltron-material.module';

@NgModule({
  declarations: [],
  imports: [],
  exports: [
    CommonModule,
    VoltronMaterialModule
  ]
})
export class SharedModule { }
