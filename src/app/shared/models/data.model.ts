export class Data {
    x: string;
    y: Array<number>;

    constructor(options: {
        x?: string,
        y?: Array<number>;
    }) {
        this.x = options.x || '';
        this.y = options.y || new Array<number>();
    }
}
