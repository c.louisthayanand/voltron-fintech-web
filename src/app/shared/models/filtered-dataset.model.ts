import { Metrics } from './metrics.model';
import { Data } from './data.model';

export class FilteredDataset {
    filtersApplied: Map<string, string>;
    metrics: Array<Metrics>;
    data: Array<Data>;

    constructor(options: {
        filtersApplied?: Map<string, string>,
        metrics?: Array<Metrics>,
        data?: Array<Data>
    }) {
        this.filtersApplied = options.filtersApplied || new Map<string, string>();
        this.metrics = options.metrics || new Array<Metrics>();
        this.data = options.data || new Array<Data>();
    }
}
