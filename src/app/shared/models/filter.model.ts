export class Filter {
    name: string;
    values: Map<string, string>;

    constructor(options: {
        name?: string,
        values?: Map<string, string>;
    }) {
        this.name = options.name || '';
        this.values = options.values || new Map<string, string>();
    }
}
