export class Metrics {
    name: string;
    index: number;
    unit: string;

    constructor(options: {
        name?: string,
        index?: number,
        unit?: string
    }) {
        this.name = options.name || '';
        this.index = options.index || 0;
        this.unit = options.unit || '';
    }
}
