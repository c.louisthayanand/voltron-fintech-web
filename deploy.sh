#!/bin/bash

npm install
npm run build
az storage blob upload-batch -d 'https://stteamowebdata.blob.core.windows.net/$web' -s ./dist/voltron-fintech-web
